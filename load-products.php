<?php
include_once 'conn.php';

if ($_POST['index'] == 0) {$sql = 'SELECT product_id, product_name, product_code, product_quantity, product_price FROM products WHERE product_quantity != 0';} 
else if ($_POST['index'] == 1) {$sql = 'SELECT product_id, product_name, product_code, product_quantity, product_price FROM products WHERE product_quantity != 0 ORDER BY product_name';}
else if ($_POST['index'] == 2) {$sql = 'SELECT product_id, product_name, product_code, product_quantity, product_price FROM products WHERE product_quantity != 0 ORDER BY product_code';}
else if ($_POST['index'] == 3) {$sql = 'SELECT product_id, product_name, product_code, product_quantity, product_price FROM products WHERE product_quantity != 0 ORDER BY product_quantity';}
else if ($_POST['index'] == 4) {$sql = 'SELECT product_id, product_name, product_code, product_quantity, product_price FROM products WHERE product_quantity != 0 ORDER BY product_price';}


$result = $conn->query($sql);

if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        echo '<tr> <th scope="row" id="productId" name="id">' . $row['product_id'] . '</th>';
        echo '<td>' . $row['product_name'] . '</td>';
        echo '<td>' . $row['product_code'] . '</td>';
        echo '<td>' . $row['product_quantity'] . '</td>';
        echo '<td>' . $row['product_price'] . ' €</td>';
    }
}

if ($_POST['index'] == 0) {$sql = 'SELECT product_id, product_name, product_code, product_quantity, product_price FROM products WHERE product_quantity = 0';} 
else if ($_POST['index'] == 1) {$sql = 'SELECT product_id, product_name, product_code, product_quantity, product_price FROM products WHERE product_quantity = 0 ORDER BY product_name';}
else if ($_POST['index'] == 2) {$sql = 'SELECT product_id, product_name, product_code, product_quantity, product_price FROM products WHERE product_quantity = 0 ORDER BY product_code';}
else if ($_POST['index'] == 3) {$sql = 'SELECT product_id, product_name, product_code, product_quantity, product_price FROM products WHERE product_quantity = 0 ORDER BY product_quantity';}
else if ($_POST['index'] == 4) {$sql = 'SELECT product_id, product_name, product_code, product_quantity, product_price FROM products WHERE product_quantity = 0 ORDER BY product_price';}

$result = $conn->query($sql);

echo '</tbody><thead class="thead-dark"><tr><th scope="col"></th><th scope="col">Vypredané produkty</th><th scope="col"></th><th scope="col"></th><th scope="col"></th></tr></thead><tbody>';

if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        echo '<tr> <th scope="row">' . $row['product_id'] . '</th>';
        echo '<td>' . $row['product_name'] . '</td>';
        echo '<td>' . $row['product_code'] . '</td>';
        echo '<td>' . $row['product_quantity'] . '</td>';
        echo '<td>' . $row['product_price'] . ' €</td>';
    }
}