<?php

include_once 'conn.php';

$sql = 'SELECT product_code FROM products';
$result = $conn->query($sql);

if ($result->num_rows > 0){
    while ($row = $result->fetch_assoc()) {
        echo '<option value="' . $row['product_code'] . '">';
    }
}